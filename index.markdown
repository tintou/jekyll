---
layout: default
title: The Zeitgeist Project
---

# Providing Desktop Activity Awareness

Zeitgeist is a service which logs the users's activities and events, anywhere from files opened to websites visited and conversations.

It makes this information readily available for other applications to use.

It is able to establish relationships between items based on similarity and usage patterns.

# Features

 * Zeitgeist logs files opened, websites visited, conversations, and emails and provides all this information over a D-Bus API
 * Track events by time to figure out exactly when and how often a user accesses something.
 * Extensions (or as we call them Smack-ins) to provide more information on desktop usage, such as geolocation, focus timer, relevancy, and much more.
