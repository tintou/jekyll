---
layout: default
title: Development
---

# Development

Most of the code is hosted on [GitHub](https://github.com/zeitgeist-project/)

Some repositories are mirrored from [freedesktop.org](https://cgit.freedesktop.org/zeitgeist/), you should not propose merge request on them but submit Git patches to the [freedesktop.org Bugzilla](https://bugs.freedesktop.org/)
