---
layout: default
title: Download
---

# Download

## Source Release

 * [You can download the latest version of Zeitgeist (1.0.1) from GitHub](https://github.com/zeitgeist-project/zeitgeist/releases/tag/v1.0.1)
 * [See older releases](https://github.com/zeitgeist-project/zeitgeist/releases)

## Development Version

The development version is available as a Git repository in the [freedesktop.org](https://cgit.freedesktop.org/zeitgeist/zeitgeist/) Website

You can clone the repository using this command:

`git clone git://anongit.freedesktop.org/zeitgeist/zeitgeist`

Go to the [development page](/development/) for more information.
